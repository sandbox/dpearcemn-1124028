Salesforce Actions is an add-on to the Salesforce Suite project.

This module will allow the creation of Advanced Actions to import or export Drupal objects to Salesforce.

Install this module as normal by copying the files to your modules folder in a folder called salesforce_actions.

Enable the module by going to your Modules page and enabling it.

Create a new advanced action to export Drupal items to Salesforce:

    Go to the actions settings page (/admin/settings/actions)
    Under "Make a new advanced action available" select "Export Drupal objects to Salesforce..." and press the Create button
    Change the description to something meaningful for this action
    Select a fieldmap from the pulldown list
    Select a minimum interval for this action (Every time, 1, 2, 4, 12 or 24 hours)
    Select whether to export all items or just those that are linked
    Select whether to export only published/active items or all items
    Click on Save

Create a new advanced action to import Drupal items from Salesforce:

    Go to the actions settings page (/admin/settings/actions)
    Under "Make a new advanced action available" select "Export Drupal objects to Salesforce..." and press the Create button
    Change the description to something meaningful for this action
    Select a fieldmap from the pulldown list
    Select a minimum interval for this action (Every time, 1, 2, 4, 12 or 24 hours)
    Select whether to link items on import
    Select a "WHERE" clause if needed
    Click on Save

Once the action is created, it can be assigned on the triggers page. Assigning these actions to the CRON page is likely (/admin/build/trigger/cron).
